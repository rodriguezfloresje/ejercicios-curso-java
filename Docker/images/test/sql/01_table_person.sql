
CREATE SEQUENCE people_seq;

CREATE TABLE person (
    id integer DEFAULT nextval('people_seq'),
    first_name varchar(50),
    last_name varchar(50),
    age int,
    email varchar(100),

    CONSTRAINT id_person PRIMARY KEY (id)
);