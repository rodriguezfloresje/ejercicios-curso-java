package Log4j.Example;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Hello world!
 *
 */
public class App 
{
	public static final Logger LOGGER = LogManager.getLogger();
    public static void main( String[] args )
    {
        LOGGER.debug( "Hello World!" );
        LOGGER.info( "Hello!" );
        LOGGER.warn( "!" );
        LOGGER.error( "H3110 W0rlt!" );
        LOGGER.fatal( "Bye World!" );
    }
}
