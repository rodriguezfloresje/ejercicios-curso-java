package mx.com.praxis.rofx.games.interfaces;

public interface Gameable {
	
	default void setGameName(String name) {
		System.out.println("Game: " + name);
	}
	void startGame();
	
}
