package mx.com.praxis.rofx.games.interfaces;

public interface Playeable {
	
	default void setGameName(String name) {
		System.out.println("Game: " + name);
	}
	void walk(double x, double y);
	
}
