package mx.com.praxis.rofx.games.interfaces;

public interface Soundable {
	
	default void setGameName(String name) {
		System.out.println("Game: " + name);
	}
	void playMusic(String song);
	
}
