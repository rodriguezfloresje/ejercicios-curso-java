package mx.com.praxis.rofx.games.main;

import java.util.Optional;

import mx.com.praxis.rofx.games.interfaces.Gameable;
import mx.com.praxis.rofx.games.interfaces.Playeable;
import mx.com.praxis.rofx.games.interfaces.Soundable;

public class PrincipalGame {
	
	private static String gameName;
	private static String sound;
	private static double playerPosX;
	private static double playerPosY;
	
	public static void main(String[] args){
		gameName = "Quisque Programo";
		sound = "wololo wololo";
		Playeable player = (x, y) -> {
			playerPosX = playerPosX + x;
			playerPosY = playerPosY + y;
		};
		Gameable game = () -> {
			player.setGameName(gameName);
		};
		Soundable music = (noise) -> {
			Optional<String> op = Optional.ofNullable(noise);
			if(!op.isPresent()) {
				noise = "defaultTrack";
			}
			System.out.println("Sound: " + noise);
		};
		game.startGame();
		player.walk(5, 4);

		System.out.println("PlayerPosition: [" + playerPosX + "," + playerPosY + "]");
		music.playMusic(sound);
	}
	
}
