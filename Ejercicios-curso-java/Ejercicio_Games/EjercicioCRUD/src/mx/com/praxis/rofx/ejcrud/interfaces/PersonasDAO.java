package mx.com.praxis.rofx.ejcrud.interfaces;

import java.sql.SQLException;
import java.util.List;

import mx.com.praxis.rofx.ejcrud.beans.Persona;

public interface PersonasDAO {
	
	public int insertPersona(Persona persona) throws SQLException;
	public int deletePersona(Persona persona) throws SQLException;
	public int deletePersona(long id) throws SQLException;
	public int updatePersona(Persona persona) throws SQLException;
	public Persona getPersona(Persona persona) throws SQLException;
	public Persona getPersona(long id) throws SQLException;
	public int insertListPersona(List<Persona> personas) throws SQLException;
	public int deleteListPersona(List<Persona> personas) throws SQLException;
	public int deleteListPersona(long[] ids) throws SQLException;
	public int updateListPersona(List<Persona> personas) throws SQLException;
	public List<Persona> getListPersona(List<Persona> personas) throws SQLException;
	public List<Persona> getListPersona(long[] ids) throws SQLException;
	
}
