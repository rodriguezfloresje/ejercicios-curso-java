package mx.com.praxis.rofx.ejcrud.daos;

import mx.com.praxis.rofx.ejcrud.beans.Persona;
import mx.com.praxis.rofx.ejcrud.interfaces.PersonasDAO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PersonasDAOImpl implements PersonasDAO {
	
	private static final String URL = "a la maquina de Thelma porfa";
	private static final String USER = "pect";
	private static final String PASS = "pect";
	
	private static final String FIRSTNAME = "firstname";
	private static final String LASTNAME = "lastname";
	private static final String AGE = "age";
	private static final String ID = "id";
	private static final String PERSONATABLE = "person";
	
	private static final String SELECTPERSONA = "SELECT " + FIRSTNAME + ", " + LASTNAME + ", " + AGE + " FROM " + PERSONATABLE + " WHERE " + ID + " = ?";
	private static final String UPDATEPERSONA = "UPDATE " + PERSONATABLE + " SET " + FIRSTNAME + " = ?, " + LASTNAME + " = ? WHERE " + ID + " = ?";
	private static final String INSERTPERSONA = "INSERT " + ID + ", " + FIRSTNAME + ", " + LASTNAME + ", " + AGE + " INTO " + PERSONATABLE + " VALUES (?,?,?,?)";
	private static final String DELETEPERSONA = "DELETE FROM " + PERSONATABLE + " WHERE " + ID + " = ?";
	
	@Override
	public int insertPersona(Persona persona) throws SQLException {
		int rowsAffected = 0;
		try(Connection connection = DriverManager.getConnection(URL, USER, PASS);
				PreparedStatement ps = connection.prepareStatement(INSERTPERSONA)) {
			try {
				ps.setLong(1, persona.getId());
				ps.setString(2, persona.getFirstName());
				ps.setString(3, persona.getLastName());
				ps.setInt(4, persona.getAge());
				rowsAffected = ps.executeUpdate();
				connection.commit();
			} catch(Exception e) {
				connection.rollback();
			}
		}
		return rowsAffected;
	}
	@Override
	public int deletePersona(Persona persona) throws SQLException {
		return deletePersona(persona.getId());
	}
	@Override
	public int deletePersona(long id) throws SQLException {
		int rowsAffected = 0;
		try(Connection connection = DriverManager.getConnection(URL, USER, PASS);
				PreparedStatement ps = connection.prepareStatement(DELETEPERSONA)) {
			try {
				ps.setLong(1, id);
				rowsAffected = ps.executeUpdate();
				connection.commit();
			} catch(Exception e) {
				connection.rollback();
			}
		}
		return rowsAffected;
	}
	@Override
	public int updatePersona(Persona persona) throws SQLException {
		int rowsAffected = 0;
		try(Connection connection = DriverManager.getConnection(URL, USER, PASS);
				PreparedStatement ps = connection.prepareStatement(UPDATEPERSONA)) {
			try {
				ps.setString(1, persona.getFirstName());
				ps.setString(2, persona.getLastName());
				ps.setLong(3, persona.getId());
				rowsAffected = ps.executeUpdate();
				connection.commit();
			} catch(Exception e) {
				connection.rollback();
			}
		}
		return rowsAffected;
	}
	@Override
	public Persona getPersona(Persona persona) throws SQLException {
		return getPersona(persona.getId());
	}
	@Override
	public Persona getPersona(long id) throws SQLException {
		Persona result = new Persona();
		try(Connection connection = DriverManager.getConnection(URL, USER, PASS);
				PreparedStatement ps = connection.prepareStatement(SELECTPERSONA)) {
			ps.setLong(1, id);
			try(ResultSet rs = ps.executeQuery()) {
				while(rs.next()) {
					result.setId(id);
					result.setFirstName(rs.getString("firstname"));
					result.setLastName(rs.getString("lastname"));
					result.setAge(rs.getInt("age"));
				}
				connection.commit();
			} catch(Exception e) {
				connection.rollback();
			}
		}
		return result;
	}
	@Override
	public int insertListPersona(List<Persona> personas) throws SQLException {
		int totalRowsAffected = 0;
		for(Persona persona : personas) {
			totalRowsAffected += insertPersona(persona);
		}
		return totalRowsAffected;
	}
	@Override
	public int deleteListPersona(List<Persona> personas) throws SQLException {
		int totalRowsAffected = 0;
		for(Persona persona : personas) {
			totalRowsAffected += deletePersona(persona);
		}
		return totalRowsAffected;
	}
	@Override
	public int deleteListPersona(long[] ids) throws SQLException {
		int totalRowsAffected = 0;
		for(long id : ids) {
			totalRowsAffected += deletePersona(id);
		}
		return totalRowsAffected;
	}
	@Override
	public int updateListPersona(List<Persona> personas) throws SQLException {
		int totalRowsAffected = 0;
		for(Persona persona : personas) {
			totalRowsAffected += updatePersona(persona);
		}
		return totalRowsAffected;
	}
	@Override
	public List<Persona> getListPersona(List<Persona> personas) throws SQLException {
		List<Persona> selectedPersons = new ArrayList<Persona>();
		for(Persona persona : personas) {
			selectedPersons.add(getPersona(persona));
		}
		return selectedPersons;
	}
	@Override
	public List<Persona> getListPersona(long[] ids) throws SQLException {
		List<Persona> selectedPersons = new ArrayList<Persona>();
		for(long id : ids) {
			selectedPersons.add(getPersona(id));
		}
		return selectedPersons;
	}
	
}
