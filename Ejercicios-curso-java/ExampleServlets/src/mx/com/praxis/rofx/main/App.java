package mx.com.praxis.rofx.main;


import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

public class App {

	public static void main(String[] args) throws Exception {

		Server server = new Server(8080);

		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);

		context.setContextPath("/servlets");

		server.setHandler(context);

		//context.addServlet(new ServletHolder(new GetAllStudentServlet()), "/all");
		//context.addServlet(new ServletHolder(new PostStudentServlet()), "/addOne");
		server.start();
	}
}
