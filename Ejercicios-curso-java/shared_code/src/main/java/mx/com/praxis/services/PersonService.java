/*
 * Copyright (c) 3/18/20
 * Mexico D.F.
 * All rights reserved.
 *
 * THIS SOFTWARE IS  CONFIDENTIAL INFORMATION PROPRIETARY OF
 * THIS INFORMATION SHOULD NOT BE DISCLOSED AND MAY ONLY BE USED IN ACCORDANCE THE TERMS DETERMINED BY THE COMPANY ITSELF.
 */
package mx.com.praxis.services;

import java.util.List;

import mx.com.praxis.dtos.PersonDto;
import mx.com.praxis.services.impl.PersonServiceImpl;

/**
 * <p>
 * </p>
 *
 * @author Marco Acevedo
 * @version Programming
 * @since Programming
 */
public interface PersonService {

	List<PersonDto> getPeople(int page, int size);

	PersonDto findById(long id);

	void update(PersonDto person);

	void insert(PersonDto person);

	static PersonService getInstance() {
		return new PersonServiceImpl();
	}
}
