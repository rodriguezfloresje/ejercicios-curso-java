/*
 * Copyright (c) 3/18/20
 * Mexico D.F.
 * All rights reserved.
 *
 * THIS SOFTWARE IS  CONFIDENTIAL INFORMATION PROPRIETARY OF
 * THIS INFORMATION SHOULD NOT BE DISCLOSED AND MAY ONLY BE USED IN ACCORDANCE THE TERMS DETERMINED BY THE COMPANY ITSELF.
 */
package mx.com.praxis.services;

import java.util.List;

import mx.com.praxis.dtos.StudentDto;
import mx.com.praxis.services.impl.StudentServiceImpl;

/**
 * <p>
 * </p>
 *
 * @author Marco Acevedo
 * @version Programming
 * @since Programming
 */
public interface StudentService {

	List<StudentDto> getStudents(int page, int size);

	StudentDto findById(long id);

	void update(StudentDto student);

	void insert(StudentDto student);

	static StudentService getInstance() {
		return new StudentServiceImpl();
	}
}
