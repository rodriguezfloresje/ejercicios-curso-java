package mx.com.praxis.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import mx.com.praxis.daos.PersonDao;
import mx.com.praxis.daos.StudentDao;
import mx.com.praxis.dtos.PersonDto;
import mx.com.praxis.dtos.StudentDto;
import mx.com.praxis.entities.Person;
import mx.com.praxis.entities.Student;
import mx.com.praxis.services.StudentService;

public class StudentServiceImpl implements StudentService {

	private final StudentDao studentDao = StudentDao.getInstance();

	private final PersonDao personDao = PersonDao.getInstance();

	@Override
	public List<StudentDto> getStudents(final int page, final int size) {
		return studentDao.findAll(page, size).stream().map(s -> toStudentDto(s)).collect(Collectors.toList());
	}

	@Override
	public StudentDto findById(final long id) {
		return toStudentDto(studentDao.findById(id));
	}

	@Override
	public void update(final StudentDto student) {
		final Student updated = new Student();

		updated.setId(student.getId());
		updated.setUniversityCareer(student.getUniversityCareer());
		updated.setYearsBachelorDegree(student.getYearsBachelorDegree());

		studentDao.update(updated);
	}

	@Override
	public void insert(final StudentDto student) {
		final Student created = new Student();

		created.setIdPerson(student.getPerson().getId());
		created.setUniversityCareer(student.getUniversityCareer());
		created.setYearsBachelorDegree(student.getYearsBachelorDegree());

		studentDao.insert(created);
	}

	private StudentDto toStudentDto(final Student student) {
		final Person person = personDao.findById(student.getIdPerson());

		final StudentDto result = StudentDto
				.builder().id(student.getId()).universityCareer(student.getUniversityCareer())
				.yearsBachelorDegree(student.getYearsBachelorDegree()).person(PersonDto.builder()
						.firstName(person.getFirstName()).id(person.getId()).lastName(person.getLastName()).build())
				.build();

		return result;
	}

}
